import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

public class ListOfStudentsGenerator {

    public static <T> void swap (T[] a, int i, int j) {
        T t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    public static HashMap<String, HashMap<String, Double>> readData(int number_of_students) {
        Scanner scan = new Scanner(System.in);
        HashMap<String, HashMap<String, Double>> data = new HashMap<>();
        String name;
        double pcm, cs, maths;
        for (int i = 0; i < number_of_students; i++) {
            System.out.println("\n***** Enter the details of student-" + (i + 1) + " *****");
            System.out.print("Name: ");
            name = scan.next();
            System.out.print("Marks of PCM: ");
            pcm = scan.nextDouble();
            System.out.print("Marks of CS: ");
            cs = scan.nextDouble();
            System.out.print("Marks of Mathematics: ");
            maths = scan.nextDouble();
            HashMap<String, Double> marks = new HashMap<>();
            marks.put("pcm", pcm);
            marks.put("cs", cs);
            marks.put("maths", maths);
            data.put(name, marks);
        }
        return data;
    }

    public static void printLists(HashMap<String, HashMap<String, Double>> data){
        ArrayList<String> computer_science = new ArrayList<>();
        ArrayList<String> biology = new ArrayList<>();
        ArrayList<String> commerce = new ArrayList<>();
        HashMap<String, Double> marks;

        for (String name : data.keySet()) {
            marks = data.get(name);
            if (marks.get("pcm") > 70 && marks.get("cs") > 80)
                computer_science.add(name);
            if (marks.get("pcm") > 70)
                biology.add(name);
            if (marks.get("maths") > 80)
                commerce.add(name);
        }

        System.out.println("\n*****List of Students that can be put into different groups*****");
        System.out.println("Computer Science: " + computer_science);
        System.out.println("Biology: " + biology);
        System.out.println("Commerce: " + commerce);
    }

    public static void printRanks(HashMap<String, HashMap<String, Double>> data){
        int number_of_students= data.size();
        String[] names = new String[number_of_students];
        Double[] total_marks = new Double[number_of_students];
        HashMap<String, Integer> ranks= new HashMap<>();
        HashMap<String, Double> marks;
        int i=0;
        for (String name : data.keySet()) {
            marks = data.get(name);
            names[i]= name;
            total_marks[i++]= (marks.get("pcm") + marks.get("cs") + marks.get("maths"));
        }

        for (i = 0; i < number_of_students-1; i++)
            for (int j = 0; j < number_of_students-i-1; j++)
                if (total_marks[j] < total_marks[j+1])
                {
                    swap(total_marks, j, j+1);
                    swap(names, j, j+1);
                    double temp = total_marks[j];
                    total_marks[j] = total_marks[j+1];
                    total_marks[j+1] = temp;

                    String t= names[j];
                    names[j]= names[j+1];
                    names[j+1]= t;
                }

        int rank=1;
        ranks.put(names[0], rank);
        for(i=1; i<number_of_students; i++){
            if(!Objects.equals(total_marks[i], total_marks[i - 1]))
                rank += 1;
            ranks.put(names[i], rank);
        }

        System.out.println(ranks);
    }

    public static void main(String[] args) {
        int number_of_students;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        number_of_students = scan.nextInt();
        HashMap<String, HashMap<String, Double>> data;
        data = readData(number_of_students);
        printLists(data);
        printRanks(data);
    }
}
